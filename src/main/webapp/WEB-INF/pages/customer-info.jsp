<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Customer Details</title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    <%@include file="/resources/css/bootstrap.css" %>
    <%@include file="/resources/css/style.css" %>
</style>
</head>
<body>
<div class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${pageContext.request.contextPath}/customerInfo/list" ><img src="http://u.cubeupload.com/surysagar/logo1.png"/></a>
	
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="#"></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">About Us</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<div class="opacitydiv" clearfix></div>
        <div class="cont">
<h1><b>Customer Purchase Information<b></h1>
<form:form method="POST" commandName="parent" action="${pageContext.request.contextPath}/customerInfo/create" >
<fmt:formatDate value="${customerInfo1.DOB}" var="dateString" pattern="dd/MM/yyyy" />

	<input type="button" class="print" style="position:absolute; right:131px;top:26px;" value="Print" onclick="location.href='${pageContext.request.contextPath}/customerInfo/print/${parent.customerPurchaseInfo.purchaseID}'" />
	
    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Agreement:</span></div>
        <div class="col-md-3"><form:input path="customerPurchaseInfo.agreement_number" class="form-control" id="cust1AggrNo" name="cust1AggrNo" placeholder="Agreement No." />
       </div>
        <div class="col-md-3"><span>Date:</span><span class="text-val"><form:input path="customerPurchaseInfo.agrement_date" style="width:100px;display:inline;margin-top:-3px; margin-left:8px;" class="form-control" id="cust1AggrNo" name="cust1AggrNo" placeholder="Date." /></span></div>
        <div class="col-md-3"><span>Reg.Office:</span><span class="text-val"><form:input path="customerPurchaseInfo.Reg_office" style="width:60px;display:inline;margin-top:-3px; margin-left:8px;" class="form-control" id="cust1AggrNo" name="cust1AggrNo" placeholder="Office." /></span></div>
        <!-- <div class="col-md-3"><form:input path="customerPurchaseInfo." class="form-control" id="cust1AggrNo" name="cust1AggrNo" placeholder="Agreement No." /> -->
        
    </div>
    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Customer Address:</span>    </div>
        <div class="col-md-8">
            <fieldset id="custType">
                <input type="radio" value="singleType"  name="custType" id="single" checked /><label for="single">Single Owner</label>               
            <input type="radio" name="custType" id="joint" value="joint" /><label for="joint">Joint Venture</label>           
            </fieldset>
        </div>
        <div class="col-md-1"></div>

    </div>
    <form:input type="hidden" path="customerPurchaseInfo.purchaseID" class="form-control" id="cust1PanId" name="cust1PanId" placeholder="Customer PAN No. *"  />
    <form:input type="hidden" path="customerInfo1.cust_id" class="form-control" id="buttonValId" name="cust1PanId" placeholder="Customer PAN No. *"  />
    <form:input type="hidden" path="customerInfo2.cust_id" class="form-control" id="cust1PanId" name="cust1PanId"  placeholder="Customer PAN No. *"  />
    <form:input type="hidden" path="customerPurchaseInfo.joint" class="form-control" id="jointId" name="cust1PanId"  placeholder="Customer PAN No. *"  />
    
    <div class="col-md-8 col-md-offset-2  addr"  >
    <div class="col-md-3 rt-align"><span></span>    </div>
    <div class="col-md-9 addrbg1">

        <div class="form-group">
       		<form:errors path="customerInfo1.Email_id" class="form-control" id="cust1Email" name="cust1Email" style="color:red" cssClass="error"/>

        	<form:input class="form-control" path="customerInfo1.name" required="required" id="cust1Name" name="cust1Name" placeholder="Enter Customer Name1 *"  />
            <form:errors path="customerInfo1.name"/>
             
             <form:input path="customerInfo1.DOB" value="${dateString}" class="form-control" placeholder="Birth date" required="required" id="cust1Bdate" name="cust1Bdate" />
             <form:input path="customerInfo1.age" class="form-control" id="cust1Age" required="required" name="cust1Age" placeholder="Customer Age*" />
        </div>
        <div class="form-group">
        	<form:textarea path="customerInfo1.Address" class="form-control" id="cust1Addr"  placeholder="Customer Address" />
        </div>
        <div class="form-group">
        	<form:input path="customerInfo1.PAN_NUMBER" required="required" class="form-control" id="cust1PanId" name="cust1PanId" placeholder="Customer PAN No. *"  />
            <form:input path="customerInfo1.phone_number" required="required" class="form-control" id="cust1Phone" name="cust1Phone"  placeholder="Customer Phone No. *"   />
            <form:input path="customerInfo1.Email_id" class="form-control" id="cust1Email" name="cust1Email" placeholder="Customer Email Id *"  />
        </div>
    </div>
    
</div>
    <div class="col-md-8 col-md-offset-2 jointven"  >
        <div class="col-md-3 rt-align"><span></span>    </div>
        <div class="col-md-9 addrbg2" >
            <div class="form-group">
                	<form:input class="form-control" path="customerInfo2.name"  id="cust2Name" name="cust2Name" placeholder="Enter Customer Name *"  />
            		<form:input path="customerInfo2.DOB" class="form-control" placeholder="Birth date" value="" id="cust1Bdate" name="cust1Bdate"  />
             		<form:input path="customerInfo2.age" class="form-control" id="cust1Age" name="cust1Age" placeholder="Customer Age*" />
          </div>

            <div class="form-group" style="margin-bottom:0px!important;">
        	<form:textarea path="customerInfo2.Address" class="form-control" id="cust1Addr"  placeholder="Customer Address" />
            </div>
            <div class="form-group">
               <form:input path="customerInfo2.PAN_NUMBER" class="form-control" id="cust1PanId" name="cust1PanId" placeholder="Customer PAN No. *"  />
          	  <form:input path="customerInfo2.phone_number" class="form-control" id="cust1Phone" name="cust1Phone"  placeholder="Customer Phone No. *"   />
         	  <form:input path="customerInfo2.Email_id" class="form-control" id="cust1Email" name="cust1Email" placeholder="Customer Email Id *"  />
       		 </div>
        </div>
        <!--<div class="col-md-4 jointven">-->

        <!--</div>-->

    </div>
    <div class="clearfix addr"></div>
    
    

    <div class="col-md-8 col-md-offset-2">
    <div class="col-md-3 rt-align"><span>Flat Details:</span></div>
    <div class="col-md-5 rt-align-only"><span>Building</span>
    	<select name="building_name" class="form-control selectcss" >
<c:forEach var="listVal" items="${flats}">

  <option>${listVal.building_name} </option>
</c:forEach>
</select>
    </div>
    <div class="col-md-4 rt-align-only"><span>Flat</span>
      <select name="flatNo" class="form-control selectcss" >
<c:forEach var="listVal" items="${flats}">

  <option>${listVal.flat_number} </option>
</c:forEach>
</select>
	</div>
	</div>
	
	<div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span></span><span class="text-val"></span></div>
        <div class="col-md-5 rt-align-only"><span>Carpet Area: </span><span class="text-val"><form:input path="flat.carpet_area" class="form-control form-tw" id="carpetArea" name="carpetArea"  /></span></div>
        <div class="col-md-4 rt-align-only"><span>Terrace Area:</span><form:input path="flat.terrace_area" class="form-control form-tw" id="terraceArea" name="terraceArea"  /></span></div>
    </div>

    

    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="form-group park">
                <span>Parking:</span>
                <fieldset id="parkingGroup">
                    <input type="radio"  value="yes"  name="parkingGroup" id="parkingYes" /><label for="parkingYes">Yes</label>
                    <input type="radio"  value="no"  name="parkingGroup" id="parkingNo" /><label for="parkingNo">No</label>
                    <input type="radio"  value="notdec"  name="parkingGroup" id="parkingNotDec" checked /><label for="parkingNotDec">Not Decided</label>
                </fieldset>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Cost Data:</span><span class="text-val"></span></div>
        <div class="col-md-5 rt-align-only"><span>Agreement value(AV)</span><span class="text-val"><form:input path="customerPurchaseInfo.agreement_value" class="form-control form-tw" id="agreeValue" name="agreeValue" /></span></div>
        <div class="col-md-4 rt-align-only"><span>Service Tax:</span><span class="text-val"><form:input path="customerPurchaseInfo.service_tax" class="form-control form-tw" id="servicetax" name="servicetax" /></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1">
        <div class="col-md-3"></div>
        <div class="col-md-5 rt-align-only"><span>VAT:</span><span class="text-val"><form:input path="customerPurchaseInfo.VAT" class="form-control form-tw" id="vat" name="vat" /></span></div>
        <div class="col-md-4 rt-align-only"></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Application Data:</span><span class="text-val"></span></div>
        <div class="col-md-5 rt-align-only"><span>Cheque No </span><span class="text-val"><form:input path="customerPurchaseInfo.cheque_number" class="form-control form-tw" id="chequeNo" name="chequeNo" /></span></div>
        <div class="col-md-4 rt-align-only"><span>Date:</span><span class="text-val"><form:input path="customerPurchaseInfo.cheque_date" class="form-control form-tw" id="chequeDate" name="chequeDate" /></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1">
        <div class="col-md-3"></div>
        <div class="col-md-5 rt-align-only"><span>Amount:</span><span class="text-val"><form:input path="customerPurchaseInfo.amount" class="form-control form-tw" id="amount" name="amount" /></span></div>
        <div class="col-md-4 rt-align-only"><span>Drawn on:</span><span class="text-val"><form:input path="customerPurchaseInfo.Drawn_on" class="form-control form-tw" id="drawnOn" name="drawnOn" /></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1 btnEvt">
        <div class="col-md-3"></div>
        <div class="col-md-9"><a><input type="submit" id="buttonValue" value="Create" onClick="onClickRadio();"/>
<input type="button" name="Agreement" value="Agreement" onclick="location.href='${pageContext.request.contextPath}/customerInfo/agreement/${parent.customerPurchaseInfo.purchaseID}'" <c:if test="${parent.customerPurchaseInfo.agreement_number!=null}"><c:out value="disabled='disabled'"/></c:if>/>
         	
     <a href="${pageContext.request.contextPath}/customerInfo/list"><input type="button" id="buttonValue" value="Back"/></a></div>
    </div>    																																					
    </div>
        <input type="hidden" id="buttonId" name="buttonName" path="customerPurchaseInfo.purchaseID" value="create">
</div>
</form:form>
  
 <script src="http://code.jquery.com/jquery-2.0.1.min.js" data-semver="2.0.1" data-require="jquery@2.0.1"></script>
 <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/2.9.0/moment.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/1/daterangepicker.js"></script>
 
 <script>
 var x = document.getElementById("buttonValId").value+"x";  
 if(x!="x")
	document.getElementById("buttonValue").setAttribute("value", "Update");
 
	 function onClickRadio(){
		 
		 if(document.getElementById("joint").checked)
			 document.getElementById("jointId").value="Y";
			 
	 }
 $(document).ready(function(){

	 /*if (document.getElementById("jointId").value=="Y"){
		 alert("hi");
		 document.getElementById("joint").checked=true;
		 var abcc = $("input[name='custType']").val();
		  abcc == "joint";
		 $(".jointven").show();
	  	 }
 */
	//radio toggle method
	
	var eq = $('.cont').height();
 	$('.opacitydiv').css({'height': $('.cont').height() + 80});
	
	if(document.getElementById("jointId").value=="Y"){
		document.getElementById("joint").checked=true;
		$("input[name='custType']").val() == "joint";		
		donechk();
		$('.opacitydiv').css({'height': $('.cont').height() + 80});
		
    }
	$(".jointven").hide();
	function donechk(){
	$("input[name='custType']").click(function(){
	    if($(this).val() == "joint") {
	    	 $(".jointven").show();
	         $('.opacitydiv').css({'height': $('.cont').height() + 80});
	    }
	    
	    else{
	        $(".jointven").hide();
	    }
	});
	donechk();
	};
	donechk();
	
	$(function() {
	    $("#cust1Bdate").datepicker({
	      changeMonth: true,//this option for allowing user to select month
	      changeYear: true //this option for allowing user to select from year range
	    });
	  });
	// Bdate cong. script
	 $(function() {
	     $('form:input[name="cust1Bdate"]').daterangepicker({
	         singleDatePicker: true,
	         showDropdowns: true
	     }, 
	     function(start, end, label) {
	         var years = moment().diff(start, 'years');
	         alert("You are " + years + " years old.");
	     });
	 });
	});
 
 
</script>

        
</body>
</html>