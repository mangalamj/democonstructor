<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    <%@include file="/resources/css/bootstrap.css" %>
    <%@include file="/resources/css/style.css" %>
</style>
<title>Customer Details</title>
</head>
<body>
<div class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${pageContext.request.contextPath}/customerInfo/list" ><img src="http://u.cubeupload.com/surysagar/logo1.png"/></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="#"></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">About Us</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<div class="opacitydiv" clearfix></div>
<div class="cont">
<h1><b>Agreement Details</b></h1>
<div id="errbox" style="display: none"></div>
<form id="myform" method="post" action="#">
<div class="container">
    <input type="hidden" value="${parent.customerPurchaseInfo.joint}" class="form-control" id="jointId" name="cust1PanId"   />
    
    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Name Of Customer:</span></div>
        <div class="col-md-3">${parent.customerInfo1.name}</div>
       <div class="col-md-3"> Ag. No. ${parent.customerPurchaseInfo.agreement_number} </div>
        <div class="col-md-3"><span>Agreement Date:</span><span class="text-val"></span></div>
    </div>
    <div class="col-md-8 col-md-offset-2 rowfield1" id="showHide">
        <div class="col-md-3 rt-align"><span>Name Of Customer 2:</span></div>
        <div class="col-md-3">${parent.customerInfo2.name}</div>
    </div>
    <div class="col-md-8 col-md-offset-2">
    <div class="col-md-3 rt-align"><span>Flat Details:</span></div>
    <div class="col-md-4"><span>Building Name:</span><span class="text-val">      ${parent.flat.building_name}</span> </div>
    <div class="col-md-4"><span>Flat No:</span><span class="text-val">      ${parent.flat.flat_number}</span> </div>

    <div class="col-md-1"></div>
</div>

    <div class="col-md-8 col-md-offset-2 rowfield1">
        <div class="col-md-3"></div>
        <div class="col-md-4"></span>
            <div class="form-group">
                <span>Carpet Area:</span><span class="text-val">       ${parent.flat.carpet_area}</span>
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group">
                <span>Terrace Area:</span><span class="text-val">&nbsp;${parent.flat.terrace_area}</span>
        </div>
        </div>
        <div class="col-md-1"> </div>
    </div>

    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="form-group">
                <span>Parking:</span><span class="text-val">     ${parent.flat.parking}</span>

            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Cost Data:</span><span class="text-val"></span></div>
        <div class="col-md-4"><span>Agreement value(AV)</span><span class="text-val">    ${parent.customerPurchaseInfo.agreement_value}</span></div>
        <div class="col-md-5"><span>Service Tax:</span><span class="text-val">    ${parent.customerPurchaseInfo.service_tax}</span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1">
        <div class="col-md-3"></div>
        <div class="col-md-5"><span>VAT:</span><span class="text-val">${parent.customerPurchaseInfo.VAT}</span></div>
        <div class="col-md-4"></div>
    </div>

    <div class="col-md-9 col-md-offset-1 rowfield1" >
        <div class="col-md-4 rt-align"><span>Application Amount Data:</span></div>
        <div class="col-md-4"><span>Cheque No </span><span class="text-val">${parent.customerPurchaseInfo.cheque_number}</span></div>
        <div class="col-md-4"><span>Date:</span><span class="text-val">${parent.customerPurchaseInfo.cheque_date}</span></div>
    </div>

    <div class="col-md-9 col-md-offset-1 rowfield1">
        <div class="col-md-4"></div>
        <div class="col-md-4"><span>Amount:</span><span class="text-val">${parent.customerPurchaseInfo.amount}</span></div>
        <div class="col-md-4"><span>Drawn on:</span><span class="text-val"></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2  tandc clearfix ">
        <p class="clearfix">
           <b> Terms and Conditions :</b></br>
1. The Agreement Value is applicable only till dd/mm/yy.</br>
2. Application amount cheque to be drawn in favour of “ATRIA Constructions”</br>
3. 15%  Amount of the Agreement Value to be deposited not later than dd/mm/yy – 7 days</br>
4. Stamp Duty, VAT and Registration Charges at applicable rate to be paid not later than dd/mm/yy – 7 days</br>
5. The amount payable is only an Application Money and does not guarantee allotment of Apartment or finalization of rates if Registration not completed till dd/mm/yy
6. No charges will be deducted from the Application Money on request for refund. However, no interest charges are payable on cancellation.
        </p>

    </div>
    <div class="col-md-8 col-md-offset-2 rowfield1 btnEvt" style="text-align:center;"><a href="${pageContext.request.contextPath}/customerInfo/list"><input type="button" id="buttonValue" value="Back"/></a></div>
    
</div>
</form>
</div>
    
       
  
 <script src="http://code.jquery.com/jquery-2.0.1.min.js" data-semver="2.0.1" data-require="jquery@2.0.1"></script>
 <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/2.9.0/moment.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/1/daterangepicker.js"></script>
 
 <script>
if(document.getElementById("jointId").value!="Y")
 document.getElementById("showHide").style.display="none"
</script>
</body>
</html>