<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
    <%@include file="/resources/css/bootstrap.css" %>
    <%@include file="/resources/css/style.css" %>
</style>
<title>Customer Details</title>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand"><img src="http://u.cubeupload.com/surysagar/logo1.png"/></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="#">Customer List</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">About Us</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>
<div class="opacitydiv" clearfix></div>
        <div class="cont">
<h1>Add / Edit Customer Information Page2</h1>
<div id="errbox" style="display: none"></div>
<form:form id="myform" method="POST" commandName="parent" action="${pageContext.request.contextPath}/customerInfo/create" >

<div class="container">
    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Agreement:</span></div>
        <div class="col-md-2"><i class="fa fa-check"></i> Done</div>
        <div class="col-md-3"><span>Date:</span><span class="text-val">23 May 2015</span></div>
        <div class="col-md-3"><span>Reg. Office:</span><span class="text-val">Pune</span></div>

    </div>
    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Customer Address:</span>    </div>
        <div class="col-md-8">
            <fieldset id="custType">
                <input type="radio" value="singleType"  name="custType" id="single" checked /><label for="single">Single Owner</label>
                <input type="radio" name="custType" id="joint" value="joint" /><label for="joint">Joint Venture</label>
            </fieldset>
        </div>
        <div class="col-md-1"></div>
    </div>
    
    <div class="col-md-8 col-md-offset-2  addr"  >
    <div class="col-md-3 rt-align"><span></span>    </div>
    <div class="col-md-9 addrbg1">
        <div class="form-group">
        	<form:input class="form-control" path="customerInfo1.name"  type="text" id="cust1Name" name="cust1Name" placeholder="Enter Customer Name1 *"  />
            <!-- <input class="form-control" type="text" id="cust1Name" name="cust1Name" placeholder="Enter Customer Name1 *" required />-->
            <form:input path="customerInfo1.DOB" class="form-control" placeholder="Birth date" value="" id="cust1Bdate" name="cust1Bdate"  />
            <!--<input class="form-control" type="text" placeholder="Birth date" value="" id="cust1Bdate" name="cust1Bdate"  data-lang="en" data-years="1940-2035" data-format="DD-MM-YYYY" />-->
            <form:input path="customerInfo1.age" class="form-control" id="cust1Age" name="cust1Age" placeholder="Customer Age*" />
            <!-- <input class="form-control" type="text" id="cust1Age" name="cust1Age" placeholder="Customer Age*" />-->
        </div>
        <div class="form-group">

        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
        	<form:textarea path="customerInfo1.Address" class="form-control" id="cust1Addr"  placeholder="Customer Address" />
            <!-- <textarea class="form-control" id="cust1Addr" type="text"  placeholder="Customer Address"></textarea>-->
        </div>
        <div class="form-group">
        	<form:input path="customerInfo1.PAN_NUMBER" class="form-control" id="cust1PanId" name="cust1PanId" placeholder="Customer PAN No. *"  />
            
            <form:input path="customerInfo1.phone_number" class="form-control" id="cust1Phone" name="cust1Phone"  placeholder="Customer Phone No. *"   />
            <!-- <input class="form-control" id="cust1Phone" name="cust1Phone" type="text" placeholder="Customer Phone No. *" required />-->
            <form:input path="customerInfo1.Email_id" class="form-control" id="cust1Email" name="cust1Email" placeholder="Customer Email Id *"  />
            <!-- <input class="form-control" id="cust1Email" name="cust1Email" type="text" placeholder="Customer Email Id *" required />-->
            
        </div>
    </div>
    <!--<div class="col-md-4 jointven">-->

    <!--</div>-->

</div>
    <div class="col-md-8 col-md-offset-2 jointven"  >
        <div class="col-md-3 rt-align"><span></span>    </div>
        <div class="col-md-9 addrbg2" >
            <div class="form-group">
                 <form:input class="form-control" path="customerInfo2.name"  type="text" id="cust2Name" name="cust2Name" placeholder="Enter Customer Name2 *"  />
                  <form:input path="customerInfo2.DOB" class="form-control" placeholder="Birth date" value="" id="cust2Bdate" name="cust2Bdate"  />
            <form:input path="customerInfo2.age" class="form-control" id="cust2Age" name="cust2Age" placeholder="Customer Age*" />
                
            <!--    <input class="form-control" type="text" id="cust2Name" name="cust2Name" placeholder="Enter Customer Name2 *" />-->
              <!--  <input class="form-control" type="text" placeholder="Birth date" value="" id="cust2Bdate" name="cust2Bdate"  data-lang="en" data-years="1940-2035" data-format="DD-MM-YYYY" />-->
               <!-- <input class="form-control" type="text" id="cust2Age" name="cust2Age" placeholder="Customer Age" />-->
            </div>

            <div class="form-group">
               <!-- <textarea class="form-control" id="cust2Addr" cust1Addr1 type="text" style=";" placeholder="Customer Address"></textarea>-->
              	<form:textarea path="customerInfo2.Address" class="form-control" id="cust2Addr"  placeholder="Customer Address" />
           
            </div>
            <div class="form-group">
               	<form:input path="customerInfo2.PAN_NUMBER" class="form-control" id="cust2PanId" name="cust2PanId" placeholder="Customer PAN No. *"  />
 	            <form:input path="customerInfo2.phone_number" class="form-control" id="cust2Phone" name="cust2Phone"  placeholder="Customer Phone No. *"   />
                <form:input path="customerInfo2.Email_id" class="form-control" id="cust2Email" name="cust2Email" placeholder="Customer Email Id *"  />
                
               <!--  <input class="form-control" id="cust2PanId" name="cust2PanId" type="text" placeholder="Customer PAN No. *" required /> -->
              <!--  <input class="form-control" id="cust2Phone" name="cust2Phone" type="text" placeholder="Customer Phone No. *" required />-->
               <!-- <input class="form-control" id="cust2Email" name="cust2Email" type="text" placeholder="Customer Email Id *" required />-->
            </div>
        </div>
        <!--<div class="col-md-4 jointven">-->

        <!--</div>-->

    </div>
    <div class="clearfix addr"></div>
    
    

    <div class="col-md-8 col-md-offset-2">
    <div class="col-md-3 rt-align"><span>Flat Details:</span></div>
    <div class="col-md-5 rt-align-only"><span>Select</span>
    	<!-- <span class="text-val form-tw" id="bldselect" >
                <div class="styled">
                    <select option="Building name">
                        <option value="" disabled selected>Building</option>
                        <option>name1</option>
                        <option>name2</option>
                        <option>name3</option>
                    </select>
                </div>
        </span> -->
        <select name="building_name" >
<c:forEach var="listVal" items="${flats}">

  <option>${listVal.building_name} </option>
</c:forEach>
</select>
    </div>
    <div class="col-md-4 rt-align-only"><span>Select</span>
      <span class="text-val form-tw" id="flatselect"><div class="styled">
                    <select option="Building name">
                        <option value="" disabled selected>Flat</option>
                        <option>name1</option>
                        <option>name2</option>
                        <option>name3</option>
                    </select>
                </div>
       </span>
	</div>
	</div>
	
	<div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span></span><span class="text-val"></span></div>
        <div class="col-md-5 rt-align-only"><span>Carpet Area: </span><span class="text-val"><form:input path="flat.carpet_area" class="form-control form-tw" id="carpetArea" name="carpetArea"  /></span></div>
        <div class="col-md-4 rt-align-only"><span>Terrace Area:</span><form:input path="flat.terrace_area" class="form-control form-tw" id="terraceArea" name="terraceArea"  /></span></div>
    </div>

    

    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="form-group park">
                <span>Parking:</span>
                <fieldset id="parkingGroup">
                    <input type="radio"  value="yes"  name="parkingGroup" id="parkingYes" /><label for="parkingYes">Yes</label>
                    <input type="radio"  value="no"  name="parkingGroup" id="parkingNo" /><label for="parkingNo">No</label>
                    <input type="radio"  value="notdec"  name="parkingGroup" id="parkingNotDec" checked /><label for="parkingNotDec">Not Decided</label>
                </fieldset>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Cost Data:</span><span class="text-val"></span></div>
        <div class="col-md-5 rt-align-only"><span>Agreement value(AV)</span><span class="text-val"><form:input path="customerPurchaseInfo.agreement_value" class="form-control form-tw" id="agreeValue" name="agreeValue" /></span></div>
        <div class="col-md-4 rt-align-only"><span>Service Tax:</span><span class="text-val"><form:input path="customerPurchaseInfo.service_tax" class="form-control form-tw" id="servicetax" name="servicetax" /></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1">
        <div class="col-md-3"></div>
        <div class="col-md-5 rt-align-only"><span>VAT:</span><span class="text-val"><form:input path="customerPurchaseInfo.VAT" class="form-control form-tw" id="vat" name="vat" /></span></div>
        <div class="col-md-4 rt-align-only"></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1" >
        <div class="col-md-3 rt-align"><span>Application Data:</span><span class="text-val"></span></div>
        <div class="col-md-5 rt-align-only"><span>Cheque No </span><span class="text-val"><form:input path="customerPurchaseInfo.cheque_number" class="form-control form-tw" id="chequeNo" name="chequeNo" /></span></div>
        <div class="col-md-4 rt-align-only"><span>Date:</span><span class="text-val"><form:input path="customerPurchaseInfo.checque_date" class="form-control form-tw" id="chequeDate" name="chequeDate" /></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1">
        <div class="col-md-3"></div>
        <div class="col-md-5 rt-align-only"><span>Amount:</span><span class="text-val"><form:input path="customerPurchaseInfo.amount" class="form-control form-tw" id="amount" name="amount" /></span></div>
        <div class="col-md-4 rt-align-only"><span>Drawn on:</span><span class="text-val"><form:input path="customerPurchaseInfo.Drawn_on" class="form-control form-tw" id="drawnOn" name="drawnOn" /></span></div>
    </div>

    <div class="col-md-8 col-md-offset-2 rowfield1 btnEvt">
        <div class="col-md-3"></div>
        <div class="col-md-5 rt-align-only"><a href="#/agreement"><input type="button" value="Submit" value="Go"> <input type="button" value="Print"> <input type="button" value="Agreement"></a></div>
        <div class="col-md-4 rt-align-only"></div>
    </div>
</div>
</form>
</div>
</form:form>
<a href="${pageContext.request.contextPath}/">Home page</a>
</body>
</html>
