package com.spr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spr.model.CustomerPurchaseInformation;



public interface CustomerInfoRepository extends JpaRepository<CustomerPurchaseInformation, Integer>{

	
	//***********changed************************
//	CustomerInfo findOne(String building_name, int flat_number);
//***************changed**********************
}
