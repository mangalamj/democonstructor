package com.spr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spr.model.Flat;

public interface FlatRepository extends JpaRepository<Flat, Integer> {

}
