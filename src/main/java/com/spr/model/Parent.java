package com.spr.model;

import javax.validation.Valid;

public class Parent{
	@Valid
	CustomerInfo customerInfo1 = new CustomerInfo();
	@Valid
	CustomerInfo customerInfo2 = new CustomerInfo();
	
	@Valid
	CustomerPurchaseInformation customerPurchaseInfo = new CustomerPurchaseInformation();
	@Valid
	Flat flat = new Flat();
	
public CustomerInfo getCustomerInfo1() {
		return customerInfo1;
	}
	public void setCustomerInfo1(CustomerInfo customerInfo1) {
		this.customerInfo1 = customerInfo1;
	}
	public CustomerInfo getCustomerInfo2() {
		return customerInfo2;
	}
	public void setCustomerInfo2(CustomerInfo customerInfo2) {
		this.customerInfo2 = customerInfo2;
	}
public CustomerPurchaseInformation getCustomerPurchaseInfo() {
		return customerPurchaseInfo;
	}
	public void setCustomerPurchaseInfo(
			CustomerPurchaseInformation customerPurchaseInfo) {
		this.customerPurchaseInfo = customerPurchaseInfo;
	}
	public Flat getFlat() {
		return flat;
	}
	public void setFlat(Flat flat) {
		this.flat = flat;
	}
	
}
