package com.spr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "customer_personal_information")
public class CustomerInfo  {

	@Id
	@GeneratedValue
	@Column(name="customer_id")
	private Integer cust_id;


	@Column(name = "Address")
	private String Address;
    
	@Column(name = "phone_number")
	private String phone_number;
	@Email
	@Column(name = "Email_id")
	private String Email_id;
	
	@Column(name = "purchase_id")
	private Integer purchaseID;
	
	@Column(name = "PAN_NUMBER")
	private String PAN_NUMBER;

	@Column(name = "name")
	private String name;
    @DateTimeFormat(pattern="dd/MM/yyyy")
	@Column(name = "DOB")
	private Date DOB;
    @Column(name = "age")
	private Integer age;
	@Column(name = "retired")
	private Integer retired;
	
	public Integer getRetired() {
		return retired;
	}

	public void setRetired(Integer retired) {
		this.retired = retired;
	}

	
	
	/*	
   	@ManyToOne
   	@JoinColumn(name="purchase_id", nullable=false)
  	private CustomerPurchaseInformation customerPurchaseInformation;
	*/
	/*public CustomerPurchaseInformation getCustomerPurchaseInformation() {
		return customerPurchaseInformation;
	}

	public void setCustomerPurchaseInformation(
			CustomerPurchaseInformation customerPurchaseInformation) {
		this.customerPurchaseInformation = customerPurchaseInformation;
	}*/

	public Integer getPurchaseID() {
		return purchaseID;
	}

	public void setPurchaseID(Integer purchaseID) {
		this.purchaseID = purchaseID;
	}
	
	public Integer getCust_id() {
		return cust_id;
	}

	public void setCust_id(Integer cust_id) {
		this.cust_id = cust_id;
	}

	public CustomerInfo() {
	}

	public String getPAN_NUMBER() {
		return PAN_NUMBER;
	}

	public void setPAN_NUMBER(String pAN_NUMBER) {
		PAN_NUMBER = pAN_NUMBER;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
	/*	 Date myDate = new Date();
		    SimpleDateFormat mdyFormat = new SimpleDateFormat("dd-MM-yyyy");
		    try {
				myDate=mdyFormat.parse(dOB);
			} catch (ParseException e) {
				e.printStackTrace();
			}*/
		    
		DOB = dOB;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getEmail_id() {
		return Email_id;
	}

	public void setEmail_id(String email_id) {
		Email_id = email_id;
	}


	
}
