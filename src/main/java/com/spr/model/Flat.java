package com.spr.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity 
@Table(name = "flat_dtl")
public class Flat implements Serializable{
	
	@Id
	@GeneratedValue
	private Integer Id;
	@Column(unique=true)
	private String building_name;
	@Column(unique=true)
	private Integer flat_number;
	@Column
	private Integer carpet_area;
	@Column
	private Integer terrace_area;
	@Column
	private String parking;
	@Column
	private int agreement;
	
	
	
	   
	public Integer getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getBuilding_name() {
		return building_name;
	}
	public void setBuilding_name(String building_name) {
		this.building_name = building_name;
	}
	public Integer getFlat_number() {
		return flat_number;
	}
	public void setFlat_number(Integer flat_number) {
		this.flat_number = flat_number;
	}
	public Integer getCarpet_area() {
		return carpet_area;
	}
	public void setCarpet_area(Integer carpet_area) {
		this.carpet_area = carpet_area;
	}
	public Integer getTerrace_area() {
		return terrace_area;
	}
	public void setTerrace_area(Integer terrace_area) {
		this.terrace_area = terrace_area;
	}
	public String getParking() {
		return parking;
	}
	public void setParking(String parking) {
		this.parking = parking;
	}
	public int getAgreement() {
		return agreement;
	}
	public void setAgreement(int agreement) {
		this.agreement = agreement;
	}
	
	

}
