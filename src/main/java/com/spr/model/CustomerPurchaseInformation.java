package com.spr.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "customer_purchase_information")
public class CustomerPurchaseInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="purchase_id")
	private Integer purchaseID;
	
	public Integer getPurchaseID() {
		return purchaseID;
	}

	public void setPurchaseID(Integer purchaseID) {
		this.purchaseID = purchaseID;
	}

		public Date getCheque_date() {
		return cheque_date;
	}

	public void setCheque_date(Date cheque_date) {
		this.cheque_date = cheque_date;
	}

//	public Flat getFlat() {
//		return flat;
//	}
//
//	public void setFlat(Flat flat) {
//		this.flat = flat;
//	}

	@Column
	private Integer agreement_number;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Column
	private Date agrement_date;
	@Column
	private String Reg_office;
	
	@Column
	private Float agreement_value;
	@Column
	private Float service_tax;
	@Column
	private Float VAT;
	@Column
	private Integer cheque_number;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Column
	private Date cheque_date;
	@Column
	private Float amount;
	@Column
	private String Drawn_on;
	@Column
	private String joint;
	
	public String getJoint() {
		return joint;
	}

	public void setJoint(String joint) {
		this.joint = joint;
	}

	@Column
	private Integer id;
	
	
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "id", insertable=false, updatable=false)
    private Flat flat;

	
	
	/*public Set<CustomerInfo> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<CustomerInfo> customers) {
		this.customers = customers;
	}*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAgreement_number() {
		return agreement_number;
	}

	public void setAgreement_number(Integer agreement_number) {
		this.agreement_number = agreement_number;
	}

	public Date getAgrement_date() {
		return agrement_date;
	}

	public void setAgrement_date(Date date) {
			this.agrement_date=date;
		}

	public String getReg_office() {
		return Reg_office;
	}

	public void setReg_office(String reg_office) {
		Reg_office = reg_office;
	}

	public Float getAgreement_value() {
		return agreement_value;
	}

	public void setAgreement_value(Float agreement_value) {
		this.agreement_value = agreement_value;
	}

	public Float getService_tax() {
		return service_tax;
	}

	public void setService_tax(Float service_tax) {
		this.service_tax = service_tax;
	}

	public Float getVAT() {
		return VAT;
	}

	public void setVAT(Float vAT) {
		VAT = vAT;
	}

	public Integer getCheque_number() {
		return cheque_number;
	}

	public void setCheque_number(Integer cheque_number) {
		this.cheque_number = cheque_number;
	}

	

	public Float getAmount() {
		return amount;
	}

	public Flat getFlat() {
		return flat;
	}

	public void setFlat(Flat flat) {
		this.flat = flat;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public String getDrawn_on() {
		return Drawn_on;
	}

	public void setDrawn_on(String drawn_on) {
		Drawn_on = drawn_on;
	}
}
