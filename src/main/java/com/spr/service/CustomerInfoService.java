package com.spr.service;

import java.util.List;

import com.spr.exception.CustomerPurchaseInformationNotFound;
import com.spr.model.CustomerInfo;
import com.spr.model.CustomerPurchaseInformation;
import com.spr.model.Flat;
import com.spr.model.Parent;

public interface CustomerInfoService {
	
	public CustomerPurchaseInformation create(Parent parent);
	public CustomerPurchaseInformation delete(int id) throws CustomerPurchaseInformationNotFound;
	public CustomerPurchaseInformation update(Parent parent) throws CustomerPurchaseInformationNotFound;
	public CustomerPurchaseInformation findById(int id);
	public List<CustomerInfo> findCustomers();
	public List<Flat> findFlats();
	public List<CustomerInfo> findAllCustomer(List<CustomerInfo> infoList,
			int purchaseId);
	public List<Parent> findAllCustomers();
//	public Information create(Information information);
//	public Information delete(int id) throws InformationNotFound;
//	public List<Information> findAll();
//	public Information update(Information information) throws InformationNotFound;
//	public Information findById(int id);
//	
//	
//	
//	
//	
//	public CustomerPurchaseInformation create(CustomerPurchaseInformation customerPurchaseInformation);
//	public CustomerPurchaseInformation delete(Integer customer_id) throws CustomerPurchaseInformationNotFound;
//	public List<CustomerPurchaseInformation> findAll1();
//	public CustomerPurchaseInformation update(CustomerPurchaseInformation customerPurchaseInformation) throws CustomerPurchaseInformationNotFound;
//	public CustomerPurchaseInformation findById(Integer customer_id);
//	
//	
//	
//	
//	
//	
//	public Flat create(Flat flat_Dtl);
//	public Flat delete(String building_name, Integer flat_number) throws FlatNotFound;
//	public List<Flat> findAll2();
//	public Flat update(Flat flat_Dtl) throws FlatNotFound;
//	public Flat findById(String building_name, Integer flat_number);
	
}



