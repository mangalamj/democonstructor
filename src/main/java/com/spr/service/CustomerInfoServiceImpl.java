package com.spr.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spr.exception.CustomerPurchaseInformationNotFound;
import com.spr.model.CustomerInfo;
import com.spr.model.CustomerPurchaseInformation;
import com.spr.model.Flat;
import com.spr.model.Parent;
import com.spr.repository.CustomerInfoRepos;
import com.spr.repository.CustomerInfoRepository;
import com.spr.repository.FlatRepository;

@Service
public class CustomerInfoServiceImpl implements CustomerInfoService {

	@Resource
	private CustomerInfoRepository customerInfoRepository;
	@Resource
	private CustomerInfoRepos customerInfoRepos;
	@Resource
	private FlatRepository flatRepository;

	EntityManager em;

	public CustomerInfoServiceImpl() {

	}

	@Override
	@Transactional
	public CustomerPurchaseInformation create(Parent parent) {
		CustomerPurchaseInformation createdCustomerPurchaseInformation = parent
				.getCustomerPurchaseInfo();
		Flat flat = parent.getFlat();
		System.out.println("------------>"+createdCustomerPurchaseInformation.getJoint()+"<-------------");
		List<Flat> flats = flatRepository.findAll();
		for (Flat dummy : flats) {
			if (dummy.getBuilding_name().equals(flat.getBuilding_name())
					&& dummy.getFlat_number() == flat.getFlat_number()) {
				flat.setId(dummy.getId());
				flat.setCarpet_area(dummy.getCarpet_area());
				flat.setParking(dummy.getParking());
				flat.setTerrace_area(dummy.getTerrace_area());
				flat.setAgreement(1);
				createdCustomerPurchaseInformation.setId(dummy.getId());
			}
		}
		flatRepository.save(flat);
	customerInfoRepository.save(createdCustomerPurchaseInformation);
		int purchaseID = createdCustomerPurchaseInformation.getPurchaseID();

		CustomerInfo customerInfo1 = parent.getCustomerInfo1();
		customerInfo1.setPurchaseID(purchaseID);
		customerInfo1.setRetired(0);
		customerInfoRepos.save(customerInfo1);
		CustomerInfo customerInfo2=new CustomerInfo() ;
		if(createdCustomerPurchaseInformation.getJoint().equals("Y"))
		{
		customerInfo2 = parent.getCustomerInfo2();
		customerInfo2.setPurchaseID(purchaseID);
		customerInfo2.setRetired(0);
		customerInfoRepos.save(customerInfo2);
		}
		return createdCustomerPurchaseInformation;
	}

	@Transactional(rollbackFor = CustomerPurchaseInformationNotFound.class)
	public CustomerPurchaseInformation delete(int id)
			throws CustomerPurchaseInformationNotFound {
		CustomerPurchaseInformation deleteCustomerPurchaseInformation = (CustomerPurchaseInformation) customerInfoRepository
				.findOne(id);
		if (deleteCustomerPurchaseInformation == null)
			throw new CustomerPurchaseInformationNotFound();
		List<CustomerInfo> infoList = customerInfoRepos.findAll();
		infoList = findAllCustomer(infoList,
				deleteCustomerPurchaseInformation.getPurchaseID());
		for (CustomerInfo list : infoList) {
			list.setRetired(1);
			customerInfoRepos.save(list);
		}
		Flat flat = flatRepository.findOne(deleteCustomerPurchaseInformation
				.getFlat().getId());
		flat.setAgreement(0);
		flatRepository.save(flat);
		customerInfoRepository.delete(deleteCustomerPurchaseInformation);
		return deleteCustomerPurchaseInformation;
	}
	

	@Override
	@Transactional
	public List<CustomerInfo> findCustomers() {
		List<CustomerInfo> infoList = customerInfoRepos.findAll();
		return infoList;
	}

	@Override
	@Transactional
	public List<Flat> findFlats() {
		List<Flat> unsoldFlats = new ArrayList<Flat>();
		List<Flat> flats = flatRepository.findAll();
		for (Flat flat : flats)
		{
			if(flat.getAgreement()!=1)
				unsoldFlats.add(flat);
		}
		return unsoldFlats;
	}

	
	@Override
	@Transactional
	public CustomerPurchaseInformation update(Parent parent)
			throws CustomerPurchaseInformationNotFound {
		CustomerPurchaseInformation customerPurchaseInformation = parent
				.getCustomerPurchaseInfo();

		customerInfoRepository.save(customerPurchaseInformation);
		int purchaseID = customerPurchaseInformation.getPurchaseID();

		CustomerInfo customerInfo1 = parent.getCustomerInfo1();
		customerInfo1.setPurchaseID(purchaseID);
		customerInfoRepos.save(customerInfo1);
		if(customerPurchaseInformation.getJoint().equals("Y"))
		{
		CustomerInfo customerInfo2 = parent.getCustomerInfo2();
		customerInfo2.setPurchaseID(purchaseID);
		
		customerInfoRepos.save(customerInfo2);
		}
		return customerPurchaseInformation;
	}

	@Override
	@Transactional
	public CustomerPurchaseInformation findById(int id) {
		return (CustomerPurchaseInformation) customerInfoRepository.findOne(id);
	}

	@Override
	@Transactional
	public List<Parent> findAllCustomers() {
		List<CustomerPurchaseInformation> purchaseList = customerInfoRepository
				.findAll();
		List<CustomerInfo> infoList = customerInfoRepos.findAll();
		List<Parent> parentList = new ArrayList<Parent>();
		
		for(CustomerPurchaseInformation purchase : purchaseList)
		{
			Parent parent = new Parent();
			parent.setCustomerPurchaseInfo(purchase);
			parent.setFlat(purchase.getFlat());
			List<CustomerInfo> shortList = findAllCustomer(infoList,purchase.getPurchaseID());
			parent.setCustomerInfo1(shortList.get(0));
			if(shortList.size()==2)
			{
				parent.setCustomerInfo2(shortList.get(1));
			}
			parentList.add(parent);
		}
	return parentList;
	}
	@Override
	@Transactional
	public List<CustomerInfo> findAllCustomer(List<CustomerInfo> infoList,
			int purchaseId) {
		List<CustomerInfo> shortList = new ArrayList<>();

		for (CustomerInfo cinfo : infoList) {
			if (cinfo.getPurchaseID() == purchaseId)
				shortList.add(cinfo);
		}
		return shortList;
	}

}
