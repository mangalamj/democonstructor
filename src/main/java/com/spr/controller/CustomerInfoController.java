package com.spr.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
//asdfsadfsad
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spr.exception.CustomerPurchaseInformationNotFound;
import com.spr.model.CustomerInfo;
import com.spr.model.CustomerPurchaseInformation;
import com.spr.model.Flat;
import com.spr.model.Parent;
import com.spr.service.CustomerInfoService;


@Controller
@RequestMapping(value="/customerInfo")
public class CustomerInfoController {
	

	@Autowired
	private CustomerInfoService customerInfoService;
	
	/*@Autowired
	private CustomerInfoValidator customerInfoValidator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(customerInfoValidator);
	}
	*/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView newCustonerInfoPage() {
		//Parent parent = new Parent();
		List<Flat> flats = customerInfoService.findFlats();
	
		ModelAndView mav = new ModelAndView("customer-info", "parent", new Parent());
		mav.addObject("flats", flats);
		return mav;
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public ModelAndView createNewCustomerInfo(@ModelAttribute @Valid Parent parent,
			BindingResult result,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request) {
		if (result.hasErrors())
		{
			System.out.println("-----------------fasa"+result.toString());
			return new ModelAndView("customer-info");
		}
		
		ModelAndView mav = new ModelAndView();
		CustomerPurchaseInformation cpinfo= parent.getCustomerPurchaseInfo();
		String building_name=request.getParameter("building_name");
		int flatNo=Integer.parseInt(request.getParameter("flatNo"));
		parent.getFlat().setBuilding_name(building_name);
		parent.getFlat().setFlat_number(flatNo);
		String message = "New customerinfo "+cpinfo.getDrawn_on()+" was successfully created.";
	mav.setViewName("redirect:/customerInfo/list");
	redirectAttributes.addFlashAttribute("message", message);	
	customerInfoService.create(parent);
	return mav;		
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView customerInfoListPage() {
		ModelAndView mav = new ModelAndView("List");
		List<Parent> parentList=customerInfoService.findAllCustomers();
			//mav.addObject("customerPurchaseInfo", newList);
		mav.addObject("parent", parentList);
		return mav;
	}

	public static final String RESOURCE="http://u.cubeupload.com/surysagar/logo1.png";  
	
	@RequestMapping(value="/agreement/{purchase_id}", method=RequestMethod.GET)
	public ModelAndView agreement(@PathVariable int purchase_id) {
	ModelAndView mav = new ModelAndView("agreement");
	Parent parent = new Parent();
	CustomerPurchaseInformation customerPurchaseInfo = customerInfoService.findById(purchase_id);
	//mav.addObject("customerPurchaseInfo", newList);
	List<CustomerInfo> list = customerInfoService.findAllCustomer(customerInfoService.findCustomers(), purchase_id);
	  parent.setCustomerInfo1(list.get(0));
	  if(list.size()==2)
	{
	parent.setCustomerInfo2(list.get(1));
	}
	  parent.setCustomerPurchaseInfo(customerPurchaseInfo);
	parent.setFlat(customerPurchaseInfo.getFlat());
	 
	mav.addObject("parent", parent);
	return mav;
	}
	@RequestMapping(value="/print/{purchase_id}", method=RequestMethod.GET)
	public ModelAndView print(@PathVariable int purchase_id) {
	Parent parent = new Parent();
	ModelAndView mav = new ModelAndView();
	CustomerPurchaseInformation customerPurchaseInformation = customerInfoService.findById(purchase_id);
	//mav.addObject("customerPurchaseInfo", newList);
	List<CustomerInfo> list = customerInfoService.findAllCustomer(customerInfoService.findCustomers(), purchase_id);
	  parent.setCustomerInfo1(list.get(0));
	  if(list.size()==2)
	{
	parent.setCustomerInfo2(list.get(1));
	}
	  parent.setCustomerPurchaseInfo(customerPurchaseInformation);
	parent.setFlat(customerPurchaseInformation.getFlat());
	CustomerPurchaseInformation customerPurchaseInfo = parent.getCustomerPurchaseInfo();
	CustomerInfo customerInfo1 = parent.getCustomerInfo1();
	CustomerInfo customerInfo2 = parent.getCustomerInfo2();
	Flat flat = parent.getFlat();
	Font font1 = FontFactory.getFont("Times-Roman", 16, Font.BOLD);
	
	Document document = new Document();
	try {
		
		 
		
		PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("/home/manglam/CustomerStatistics.pdf"));
		document.open();
		
	      Font f1=new Font(FontFamily.TIMES_ROMAN,18.0f,Font.UNDERLINE);
	      Paragraph p1 = new Paragraph("Customer Information Sheet", f1);
	      p1.setAlignment(Element.ALIGN_CENTER);
	      p1.setFont(font1);
	      document.add(p1);
	      document.add(new Phrase("\n"));
	      
	      Font f2=new Font(FontFamily.TIMES_ROMAN,15.0f,Font.UNDERLINE);
	      Paragraph p2 = new Paragraph("Customer Data:" ,f2);
	      p2.setAlignment(Element.ALIGN_LEFT);
	      document.add(p2);
	      document.add(new Phrase("\n"));
		
	      Font f4=new Font(FontFamily.TIMES_ROMAN,12.0f);
	     
	      if(customerPurchaseInfo.getAgrement_date()!=null)
	      {
	      SimpleDateFormat format0 = new SimpleDateFormat("dd/MM/yyyy");
	      System.out.println("--------------------------------->"+customerPurchaseInfo.getAgrement_date());
	      String DateToStr0 = format0.format(customerPurchaseInfo.getAgrement_date());
	      Paragraph p4 = new Paragraph("Agreement Number	:"+ "	"+customerPurchaseInfo.getAgreement_number()	  +  "				" +    	"Date	:"+"	"+DateToStr0 +           "				" +      			"Reg Office	:"+"	"+customerPurchaseInfo.getReg_office(),f4);
	      p4.setAlignment(Element.ALIGN_LEFT);
	      document.add(p4);
	      document.add(new Phrase("\n"));
	      }
		 
	if(customerPurchaseInfo.getJoint().equals("Y"))
		 document.add(createCustomerTable(customerInfo1, customerInfo2));  
		
	else	 
		document.add(createCustomerInfoTable1(customerInfo1));
	document.add(new Phrase("\n"));
		  
		  
		  Font f5=new Font(FontFamily.TIMES_ROMAN,15.0f,Font.UNDERLINE);
		  Paragraph p5 = new Paragraph("Flat Details :",f5);
		  p5.setAlignment(Element.ALIGN_LEFT);
	      document.add(p5);
	      
	      Font f6=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph  p6= new Paragraph("Building 		 		   : "+ flat.getBuilding_name(),f6);
	      p6.setAlignment(Element.ALIGN_LEFT);
	      document.add(p6);
	      
	      Font f7=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p7= new Paragraph("Flat Number		 : "+flat.getFlat_number(),f7);
	      p7.setAlignment(Element.ALIGN_LEFT);
	      document.add(p7);
	      
	      Font f8=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p8 = new Paragraph("Carpet Area	   : "+flat.getCarpet_area(),f8);
	      p8.setAlignment(Element.ALIGN_LEFT);
	      document.add(p8);
	      
	      Font f9=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p9 = new Paragraph("Terrace Area 	: "+flat.getTerrace_area(),f9);
	      p9.setAlignment(Element.ALIGN_LEFT);
	      document.add(p9);
	      
	      Font f10=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p10 = new Paragraph("Parking		        : "+ flat.getParking(),f10);
	      p10.setAlignment(Element.ALIGN_LEFT);
	      document.add(p10);
	      document.add(new Phrase("\n"));
	      
	      Font f11=new Font(FontFamily.TIMES_ROMAN,15.0f,Font.UNDERLINE);
	      Paragraph p11= new Paragraph("Cost Data :	",f11);
	      p11.setAlignment(Element.ALIGN_LEFT);
	      document.add(p11);
	      
	      Font f12=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph  p12= new Paragraph("Agreement Value (AV) :	"+customerPurchaseInfo.getAgreement_value(),f12);
	      p12.setAlignment(Element.ALIGN_LEFT);
	      document.add(p12);
	      
	      Font f13=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p13 = new Paragraph("Service Tax	                   :	"+customerPurchaseInfo.getService_tax(),f13);
	      p13.setAlignment(Element.ALIGN_LEFT);
	      document.add(p13);
	      
	      Font f14=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p14 = new Paragraph("VAT	                             	:	"+customerPurchaseInfo.getVAT(),f14);
	      p14.setAlignment(Element.ALIGN_LEFT);
	      document.add(p14);
	      document.add(new Phrase("\n"));
	      
	      Font f15=new Font(FontFamily.TIMES_ROMAN,15.0f,Font.UNDERLINE);
	      Paragraph p15= new Paragraph("Application Amount Data",f15);
	      p15.setAlignment(Element.ALIGN_LEFT);
	      document.add(p15);
	      
	      Font f16=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p16= new Paragraph("Cheque No :	"+customerPurchaseInfo.getCheque_number(),f16);
	      p16.setAlignment(Element.ALIGN_LEFT);
	      document.add(p16);
	      
	      Font f17=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	      String DateToStr = format.format(customerPurchaseInfo.getCheque_date());
	      Paragraph p17= new Paragraph("Date	          :	"+DateToStr,f17);
	      p17.setAlignment(Element.ALIGN_LEFT);
	      document.add(p17);
	      
	      Font f18=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p18= new Paragraph("Amount     :	"+customerPurchaseInfo.getAmount(),f18);
	      p18.setAlignment(Element.ALIGN_LEFT);
	      document.add(p18);
	      
	      Font f19=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p19= new Paragraph("Drawn on  :	"+customerPurchaseInfo.getDrawn_on(),f19);
	      p19.setAlignment(Element.ALIGN_LEFT);
	      document.add(p19);
	      document.add(new Phrase("\n"));
	      
	      Font f20=new Font(FontFamily.TIMES_ROMAN,15.0f,Font.UNDERLINE);
	      Paragraph p20= new Paragraph("Terms and Conditions :	",f20);
	      p20.setAlignment(Element.ALIGN_LEFT);
	      document.add(p20);
	      	
	      Font f21=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p21= new Paragraph("		1. The Agreement Value is applicable only till dd/mm/yy. ",f21);
	      p21.setAlignment(Element.ALIGN_LEFT);
	      document.add(p21);
	      
	      Font f22=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p22= new Paragraph("		2. Application amount cheque to be drawn in favour of â€œATRIA Constructionsâ€�",f22);
	      p22.setAlignment(Element.ALIGN_LEFT);
	      document.add(p22);
	      
	      Font f23=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p23= new Paragraph("		3. 15%  Amount of the Agreement Value to be deposited not later than dd/mm/yy â€“ 7 days",f23);
	      p23.setAlignment(Element.ALIGN_LEFT);
	      document.add(p23);
	      
	      Font f24=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p24= new Paragraph("		4. Stamp Duty, VAT and Registration Charges at applicable rate to be paid not later than dd/mm/yy â€“ 7 days",f24);
	      p24.setAlignment(Element.ALIGN_LEFT);
	      document.add(p24);
	      
	      Font f25=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p25= new Paragraph("		5. The amount payable is only an Application Money and does not guarantee allotment of Apartment or finalization of rates if Registration not completed till dd/mm/yy",f25);
	      p25.setAlignment(Element.ALIGN_LEFT);
	      document.add(p25);
	      
	      Font f26=new Font(FontFamily.TIMES_ROMAN,12.0f);
	      Paragraph p26= new Paragraph("		6. No charges will be deducted from the Application Money on request for refund. However, no interest charges are payable on cancellation. ",f26);
	      p26.setAlignment(Element.ALIGN_LEFT);
	      document.add(p26);
	      
		
		document.close();
		writer.close();
	
	} catch (DocumentException e) {
		e.printStackTrace();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	mav.setViewName("redirect:/customerInfo/list");
	return mav;
}




private static Element createCustomerTable(CustomerInfo custInfo1, CustomerInfo custInfo2) {
//      PdfPTable table = new PdfPTable(3);
    PdfPTable table = new PdfPTable(2);
    table.setHorizontalAlignment(Element.ALIGN_LEFT);
//      PdfPCell cell = new PdfPCell(new Phrase("Cell with colspan 3"));\
    Font f27=new Font(FontFamily.TIMES_ROMAN,12.0f);
    PdfPCell cell = new PdfPCell(new Phrase("Customer",f27));
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(cell);
    
    Font f28=new Font(FontFamily.TIMES_ROMAN,12.0f);
    cell = new PdfPCell(new Phrase("Customer",f28));
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(cell);
//     cell.setColspan(3);
//       table.addCell(cell);
    // now we add a cell with rowspan 2
//       cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
//    cell.setRowspan(2);
//    table.addCell(cell);
    // we add the four remaining cells with addCell()
    table.addCell(new Phrase("Name	:	"+custInfo1.getName(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Name	:	"+custInfo2.getName(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    
    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    String DateToStr1 = format.format(custInfo1.getDOB());
    String DateToStr2 = format.format(custInfo2.getDOB());

    table.addCell(new Phrase("Date Of Birth	:	"+DateToStr1, FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Date Of Birth	:	"+DateToStr2, FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
//    table.addCell("Date Of Birth:"+DateToStr1);
//    table.addCell("Date Of Birth:"+DateToStr2);
    table.addCell(new Phrase("Age	:	"+custInfo1.getAge(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Age	:	"+custInfo2.getAge(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
//    table.addCell("Age:"+custInfo1.getAge());
//    table.addCell("Age:"+custInfo2.getAge());
    table.addCell(new Phrase("Address	:	"+custInfo1.getAddress(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Address	:	"+custInfo2.getAddress(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
//    table.addCell("Address:"+custInfo1.getAddress());
//    table.addCell("Address:"+custInfo2.getAddress());
    table.addCell(new Phrase("PAN Number	:	"+custInfo1.getPAN_NUMBER(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("PAN Number	:	"+custInfo2.getPAN_NUMBER(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
//    table.addCell("PAN Number :"+custInfo1.getPAN_NUMBER());
//    table.addCell("PAN Number :"+custInfo2.getPAN_NUMBER());
    table.addCell(new Phrase("Phone Number	:	"+custInfo1.getPhone_number(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Phone Number	:	"+custInfo2.getPhone_number(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
//    table.addCell("Phone Number :"+custInfo1.getPhone_number());
//    table.addCell("Phone Number :"+custInfo2.getPhone_number());
    table.addCell(new Phrase("Email id	:	"+custInfo1.getEmail_id(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Email id	:	"+custInfo2.getEmail_id(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
//    table.addCell("Email id  :"+custInfo1.getEmail_id());
//    table.addCell("Email id  :"+custInfo2.getEmail_id());
    return table;
}

private static Element createCustomerInfoTable1(CustomerInfo custInfo1	){
	
    PdfPTable table = new PdfPTable(2);
    table.setHorizontalAlignment(Element.ALIGN_LEFT);
    Font f27=new Font(FontFamily.TIMES_ROMAN,12.0f);
    PdfPCell cell = new PdfPCell(new Phrase("Customer",f27));
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(cell);
    
    Font f28=new Font(FontFamily.TIMES_ROMAN,12.0f);
    cell = new PdfPCell(new Phrase("Customer",f28));
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(cell);
    table.addCell(new Phrase("Name	:	"+custInfo1.getName(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    String DateToStr1 = format.format(custInfo1.getDOB());

    table.addCell(new Phrase("Date Of Birth	:	"+DateToStr1, FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Age	:	"+custInfo1.getAge(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Address	:	"+custInfo1.getAddress(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("PAN Number	:	"+custInfo1.getPAN_NUMBER(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Phone Number	:	"+custInfo1.getPhone_number(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    table.addCell(new Phrase("Email id	:	"+custInfo1.getEmail_id(), FontFactory.getFont(FontFactory.TIMES_ROMAN,12.0f)));
    return table;
	
}




	
	
	@RequestMapping(value="/edit/{purchase_id}", method=RequestMethod.GET)
	public ModelAndView editCustomerInfoPage(@PathVariable int purchase_id) {
		ModelAndView mav = new ModelAndView();
		Parent parent = new Parent();
		CustomerPurchaseInformation customerPurchaseInfo = customerInfoService.findById(purchase_id);
		List<CustomerInfo> list = customerInfoService.findAllCustomer(customerInfoService.findCustomers(), purchase_id);
	   	parent.setCustomerInfo1(list.get(0));
	   	if(list.size()==2)
		{
			parent.setCustomerInfo2(list.get(1));
		}
	   	parent.setCustomerPurchaseInfo(customerPurchaseInfo);
		parent.setFlat(customerPurchaseInfo.getFlat());
		List<Flat> flats= new ArrayList<>();
		flats.add(customerPurchaseInfo.getFlat());
		mav.addObject("parent", parent);
		mav.addObject("flats", flats);
		mav.setViewName("customer-info");
		return mav;
	}
	
	@RequestMapping(value="/edit/{customer_id}", method=RequestMethod.POST)
	public ModelAndView editCustomerInfo(@ModelAttribute @Valid Parent parent,
			BindingResult result,
			@PathVariable int customer_id, 
			final RedirectAttributes redirectAttributes) throws CustomerPurchaseInformationNotFound {
		ModelAndView mav = new ModelAndView("redirect:/index.html");
		String message = "CustomerInfo was successfully updated.";
		customerInfoService.update(parent);
		redirectAttributes.addFlashAttribute("message", message);	
		return mav;
	}
	
	@RequestMapping(value="/delete/{customer_id}", method=RequestMethod.GET)
	public ModelAndView deleteCustomerInfo(@PathVariable int customer_id,
			final RedirectAttributes redirectAttributes) throws CustomerPurchaseInformationNotFound {
		
		ModelAndView mav = new ModelAndView("redirect:/customerInfo/list");		
		
		customerInfoService.delete(customer_id);
		
		String message ="";
		redirectAttributes.addFlashAttribute("message", message);
		return mav;
	}
	
}
